FROM ubuntu:20.04
#as build-stage

ENV DEBIAN_FRONTEND=noninteractive

# Install prerequisites
RUN apt -y update && apt -y upgrade && \
    apt -y install autoconf automake autotools-dev curl python3 libmpc-dev libmpfr-dev \
           libgmp-dev gawk build-essential bison flex texinfo gperf libtool \
           patchutils bc zlib1g-dev libexpat-dev git \
           pkg-config ninja-build \
           libglib2.0-dev libpixman-1-dev

# Build toolchain
WORKDIR /riscv/
RUN git clone https://github.com/riscv-collab/riscv-gnu-toolchain.git
WORKDIR /riscv/riscv-gnu-toolchain/
RUN ./configure --prefix=/opt/riscv
RUN make -j $(nproc) linux
RUN make -j $(nproc) build-qemu
RUN make install

#FROM ubuntu:20.04 as production-stage
#COPY --from=build-stage /opt/riscv /opt/riscv
ENV PATH="/opt/riscv/bin:${PATH}"
ENV RISCV="/opt/riscv/"

# Test if requirements exists
RUN command -v riscv64-unknown-linux-gnu-gcc
CMD ["bash"]
