#include <stdio.h>
#include <stdlib.h>

/*
编译出能支持+和-的程序

   .global main
main:
   li a0, 1
   addi a0, a0, -2
   addi a0, a0, 3
   ret
*/

int main(int argc, char **argv) {
    // 判断输入的参数是否为2个，argv[0]是程序名字
    // argv[1] 是输入的参数
    if (argc != 2){
        // 异常处理，提示参数不对
        // fprintf, 格式化输出，往文件内写入字符串
        // stderr 异常文件(linux一切皆文件)，用于往屏幕显示异常信息
        fprintf(stderr, "%s: invalid number of arguments\n", argv[0]);
        // 程序返回值不为0时，表示存在错误
        return 1;
    }
    // 使用p表示字符串
    char *p = argv[1];

    printf("  .globl main\n");
    printf("main:\n");
    // 解析第一个数字
    // 假设我们的算是 num (op num) (op num) ...的形式
    printf("   li a0, %ld\n", strtol(p, &p, 10));

    // 解析 (op num)
    while(*p) {
        // 解析 op
        if (*p == '+'){
            ++p;
            printf("   addi a0, a0, %ld\n", strtol(p, &p, 10));
        }else if (*p == '-'){
            ++p;
            // addi 中imm为有符号立即数，所以减法表示为rd = rs1 + (-imm)
            // addi 中imm最大啊为256 如果大于会溢出
            printf("   addi a0, a0, -%ld\n", strtol(p, &p, 10));
        }else {
            fprintf(stderr, "unexpected character: '%c'\n", *p);
            return 1;
        }
    } 
    // ret为 jalr x0, x1 0别名指令，用于返回子程序
    printf("  ret\n");
    return 0;

}