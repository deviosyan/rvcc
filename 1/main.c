#include <stdio.h>
#include <stdlib.h>

/*
编译出能返回特定数值的程序
*/
int main(int argc, char **argv) {
    // 判断输入的参数是否为2个，argv[0]是程序名字
    // argv[1] 是输入的参数
    if (argc != 2){
        // 异常处理，提示参数不对
        // fprintf, 格式化输出，往文件内写入字符串
        // stderr 异常文件(linux一切皆文件)，用于往屏幕显示异常信息
        fprintf(stderr, "%s: invalid number of arguments\n", argv[0]);
        // 程序返回值不为0时，表示存在错误
        return 1;
    }
    // 生命一个全局main段，同时也是程序段的入口
    printf("  .globl main\n");
    printf("main:\n");
    // li为addi别名指令，加载一个立即数到寄存器中
    // 传入程序的参数为str类型，因为需要转换为需要int类型
    // atoi为ASCII to integer
    printf("  li a0, %d\n", atoi(argv[1]));
    // ret为 jalr x0, x1 0别名指令，用于返回子程序
    printf("  ret\n");
    return 0;

}